﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_GridView_EditUpdateDelete.Models
{
    public class PersonEntity
    {
        public decimal? PersonId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }
    }
}