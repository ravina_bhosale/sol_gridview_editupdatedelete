﻿using Sol_GridView_EditUpdateDelete.DAL.ORD;
using Sol_GridView_EditUpdateDelete.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_GridView_EditUpdateDelete.DAL
{
    public class PersonDal
    {
        #region Declaration
        private Lazy<PersonDcDataContext> dc = null;
        #endregion

        #region Constructor
        public PersonDal()
        {
            dc = new Lazy<PersonDcDataContext>(() => new PersonDcDataContext());
        }
        #endregion

        #region Public Method
        public async Task<Boolean> UpdatePersonData(PersonEntity personEntityObj)
        {
            int? status = null;
            string message = null;

            return await Task.Run(() =>
            {
                var setQuery=
                dc
                ?.Value
                ?.uspPerson
                (
                    "Update",
                    personEntityObj?.PersonId,
                    personEntityObj?.FirstName,
                    personEntityObj?.LastName,
                    ref status,
                    ref message
                 );
                return (status == 1) ? true : false;
            });
        }

        public async Task<Boolean> DeletePersonData(PersonEntity personEntityObj)
        {
            int? status = null;
            string message = null;

            return await Task.Run(() =>
            {
                var setQuery =
                dc
                ?.Value
                ?.uspPerson
                (
                    "Delete",
                    personEntityObj?.PersonId,
                    personEntityObj?.FirstName,
                    personEntityObj?.LastName,
                    ref status,
                    ref message
                 );
                return (status == 1) ? true : false;
            });
        }

        public async Task<IEnumerable<PersonEntity>> SelectPersonData()
        {

            return await Task.Run(() =>
            {
                return
                dc
                ?.Value
                ?.uspGetPerson()
                 ?.AsEnumerable()
                 ?.Select((leuspPersonObj) => new PersonEntity()
                 {
                     PersonId = leuspPersonObj?.PersonId,
                     FirstName = leuspPersonObj?.FirstName,
                     LastName = leuspPersonObj?.LastName
                 }).ToList();

            });
        }

        public async Task<IEnumerable<PersonEntity>> GetPersonData(int startRowIndex, int maximumRows)
        {
            return await Task.Run(async () => {

                return
                    (await this.SelectPersonData())
                    ?.AsEnumerable()
                    ?.Skip(startRowIndex)
                    ?.Take(maximumRows)
                    ?.ToList();

            });
        }

        public async Task<int?> GetTotalCount()
        {
            return await Task.Run(async () => {

                return
                    (await this.SelectPersonData())
                    ?.AsEnumerable()
                    ?.Count();

            });
        }

        public async Task<PersonEntity> FindPersonData(int id)
        {
            return await Task.Run(async () => {

                return
                    (await this.SelectPersonData())
                    ?.AsEnumerable()
                    ?.FirstOrDefault((lePerasonEntityObj) => lePerasonEntityObj.PersonId == id);

            });
        }
        #endregion
    }
}