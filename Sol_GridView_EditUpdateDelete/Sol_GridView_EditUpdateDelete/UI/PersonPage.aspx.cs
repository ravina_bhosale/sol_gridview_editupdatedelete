﻿using Sol_GridView_EditUpdateDelete.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_GridView_EditUpdateDelete.UI
{
    public partial class PersonPage : System.Web.UI.Page
    {
        #region declaration
        private PersonDal personDalObj = null;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Public Methods
        public async Task<SelectResult> BindPersonData(int startRowIndex, int maximumRows)
        {
            // Create an Instance of EMployee Dal
            personDalObj = new PersonDal();

            // Return data to Grid View
            return
                   new SelectResult(
                        Convert.ToInt32(await personDalObj?.GetTotalCount()),
                        await personDalObj?.GetPersonData(startRowIndex, maximumRows)
                        );
        }
        #endregion

        // The id parameter name should match the DataKeyNames value set on the control
        public async Task gvPerson_UpdateItem(int PersonId)
        {
            Sol_GridView_EditUpdateDelete.Models.PersonEntity item = null;
            // Load the item here, e.g. item = MyDataLayer.Find(id);

            item = await new PersonDal().FindPersonData(PersonId);

            if (item == null)
            {
                // The item wasn't found
                ModelState.AddModelError("", String.Format("Item with id {0} was not found", PersonId));
                return;
            }
            TryUpdateModel(item);
            if (ModelState.IsValid)
            {
                // Save changes here, e.g. MyDataLayer.SaveChanges();
                await new PersonDal().UpdatePersonData(item);
            }
        }

        // The id parameter name should match the DataKeyNames value set on the control
        public async Task gvPerson_DeleteItem(int PersonId)
        {
            Sol_GridView_EditUpdateDelete.Models.PersonEntity item = null;
            item = await new PersonDal().FindPersonData(PersonId);

            if (item == null)
            {
                // The item wasn't found
                ModelState.AddModelError("", String.Format("Item with id {0} was not found", PersonId));
                return;
            }
            TryUpdateModel(item);
            if (ModelState.IsValid)
            {
                await new PersonDal().DeletePersonData(item);
            }

        }
    }
}