﻿CREATE TABLE [dbo].[tblPerson] (
    [PersonId]  NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [LastName]  VARCHAR (50) NULL,
    CONSTRAINT [PK_tblPerson] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);

