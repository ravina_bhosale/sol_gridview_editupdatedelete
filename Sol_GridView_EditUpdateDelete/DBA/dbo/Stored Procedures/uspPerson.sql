﻿CREATE PROCEDURE uspPerson

	@Command Varchar(50)=NULL,
	
	@PersonId Numeric(18,0)=NULL,
	@FirstName Varchar(50)=NULL,
	@LastName Varchar(50)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT

	AS
	BEGIN
		DECLARE @ErrorMessage varchar(MAX)

			IF @Command='Update'
				BEGIN 
					BEGIN TRANSACTION

					BEGIN TRY

					SELECT 
						@FirstName=CASE WHEN @FirstName IS NULL THEN P.FirstName ELSE @FirstName END,
						@LastName=CASE WHEN @LastName IS NULL THEN P.LastName ELSE @LastName END
						FROM tblPerson as P
							WHERE P.PersonId=@PersonId

						UPDATE tblPerson
						SET FirstName=@FirstName,
							LastName=@LastName
						WHERE PersonId=@PersonId

						SET @Status=1
						SET @Message='Update Successfully'

						COMMIT TRANSACTION
					 END TRY

					 BEGIN CATCH
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION

						SET @Status=0
						SET @Message='There is some error'

						RAISERROR(@ErrorMessage,16,1)
					 END CATCH
				END

			
			ELSE IF @Command='Delete'
			BEGIN
					BEGIN TRANSACTION

			BEGIN TRY
				DELETE FROM tblPerson
						WHERE PersonId=@PersonId

						SET @Status=1
						SET @Message='Delete Successful'

				COMMIT TRANSACTION
			END TRY

			BEGIN CATCH
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				SET @Status=0
				SET @Message='There is some error'

				RAISERROR(@ErrorMessage,16,1)
			END CATCH
		END
	END

